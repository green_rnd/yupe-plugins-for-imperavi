if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.tableEdit = function () {
    var td = "";
    var table = "";

    return {
        init: function () {
            var dropdown = {};

            dropdown.tdOptions = {title: "Свойства ячейки", func: this.tableEdit.tdOptions};
            dropdown.tableOptions = {title: "Свойства таблицы", func: this.tableEdit.tableOptions};
            dropdown.tdSpan = {title: "Объединить ячейки", func: this.tableEdit.tdSpan};
            dropdown.tdSpanDel = {title: "Убрать объединение", func: this.tableEdit.tdSpanDel};
            dropdown.tdDel = {title: "Удалить ячейку", func: this.tableEdit.tdDel};

            dropdown.tdSpanLeft = {title: "Объединить с левой", func: this.tableEdit.tdSpanLeft};
            dropdown.tdSpanRight = {title: "Объединить с правой", func: this.tableEdit.tdSpanRight};
            dropdown.tdSpanTop = {title: "Объединить с верхней", func: this.tableEdit.tdSpanTop};
            dropdown.tdSpanBottom = {title: "Объединить с нижней", func: this.tableEdit.tdSpanBottom};

            this.observe.addButton('td', 'tableEdit');
            this.observe.addButton('th', 'tableEdit');

            var button = this.button.addBefore('link', 'tableEdit', "Свойства таблицы");
            this.button.addDropdown(button, dropdown);

            $(document).on('change', '.tableEditForm select, .tableEditForm input', function () {
                $(this).addClass('touched');
            });
        },
        tdOptions: function () {
            td = this.tableEdit.getTd();

            if (!td) {
                return false;
            }

            var template = '<section id="td-options-wrapper" class="tableEditForm">';

            template += '<div class="te-row">';

            template += '<label for="tdValign">Вертикальное выравнивание' +
                '<select name="tdValign" id="tdValign" value="">' +
                '<option value="none">none</option>' +
                '<option value="top">top</option>' +
                '<option value="middle">middle</option>' +
                '<option value="bottom">bottom</option>' +
                '</select></label>';

            template += '</div>';

            template += '<label for="tdBackgroundColor">Цвет ячейки <input class="preview" type="text" name="tdBackgroundColor" id="tdBackgroundColor" value="#000"/></label>';

            template += '<div class="te-row">';

            template += '<label for="tdBorderWidth">Толщина границы <input type="text" name="tdBorderWidth" id="tdBorderWidth" value=""/></label>';
            template += '<label for="tdBorderColor">Цвет границы <input type="text" name="tdBorderColor" id="tdBorderColor" value=""/></label>';
            template += '<label for="tdBorderStyle">Стиль границы' +
                '<select name="tdBorderStyle" id="tdBorderStyle" value="">' +
                '<option value="none">нет</option>' +
                '<option value="solid">_______</option>' +
                '<option value="dashed">_ _ _ _</option>' +
                '<option value="dotted">. . . . . </option>' +
                '</select></label>';

            template += '</div>';

            template += '<div class="te-row">';

            template += '<label for="tdOutlineWidth">Толщина внешней границы <input type="text" name="tdOutlineWidth" id="tdOutlineWidth" value=""/></label>';
            template += '<label for="tdOutlineColor">Цвет внешней границы <input type="text" name="tdOutlineColor" id="tdOutlineColor" value=""/></label>';
            template += '<label for="tdOutlineStyle">Стиль границы' +
                '<select name="tdOutlineStyle" id="tdOutlineStyle" value="">' +
                '<option value="none">нет</option>' +
                '<option value="solid">_______</option>' +
                '<option value="dashed">_ _ _ _</option>' +
                '<option value="dotted">. . . . . </option>' +
                '</select></label>';

            template += '</div>';

            template += '<div class="te-row">';

            template += '<label for="tdColspan">Объединяет колонок <input type="text" name="tdColspan" id="tdColspan" value=""/></label>';
            template += '<label for="tdRowspan">Объединяет строк <input type="text" name="tdRowspan" id="tdRowspan" value=""/></label>';

            template += '</div>';

            template += '<div class="te-row">';

            template += '<label for="tdWidth">Ширина ячейки <input type="text" name="tdWidth" id="tdWidth" value=""/></label>';
            template += '<label for="tdHeight">Высота ячейки <input type="text" name="tdHeight" id="tdHeight" value=""/></label>';
            template += '<label for="tdClear">Очистить оформление ячейки <input type="checkbox" name="tdClear" id="tdClear" /></label>';

            template += '</div>';

            template += '</section>';

            this.modal.addTemplate('tdOptions', template);
            this.modal.load('tdOptions', 'Свойства ячейки', 600);

            this.modal.createCancelButton();

            var button = this.modal.createActionButton("Ok");

            button.on('click', this.tableEdit.setTdOptions);

            this.modal.show();
            this.tableEdit.getTdOptions();

        },
        tableOptions: function () {
            table = this.tableEdit.getTable();

            if (!table) {
                return false;
            }

            var template = '<section id="table-options-wrapper" class="tableEditForm">';

            template += '<label for="tableBackgroundColor">Цвет фона таблицы <input class="preview" type="text" name="tableBackgroundColor" id="tableBackgroundColor" value="#000"/></label>';

            template += '<label for="tableCenter">Выровнять таблицу по центру ' +
                '<input type="checkbox" name="tableCenter" id="tableCenter" style="vertical-align: text-bottom;" />' +
                '</label>';

            template += '<div class="te-row">';

            template += '<label for="tableBorderWidth">Толщина границы <input type="text" name="tableBorderWidth" id="tableBorderWidth" value=""/></label>';
            template += '<label for="tableBorderColor">Цвет границы <input type="text" name="tableBorderColor" id="tableBorderColor" value=""/></label>';
            template += '<label for="tableBorderStyle">Стиль границы' +
                '<select name="tableBorderStyle" id="tableBorderStyle" value="">' +
                '<option value="none">нет</option>' +
                '<option value="solid">_______</option>' +
                '<option value="dashed">_ _ _ _</option>' +
                '<option value="dotted">. . . . . </option>' +
                '</select></label>';

            template += '<label for="tableAllBorders">Применить также ко всем внутренним границам ' +
                '<input type="checkbox" name="tableAllBorders" id="tableAllBorders" style="vertical-align: text-bottom;" />' +
                '</label>';

            template += '</div>';

            template += '<div class="te-row">';

            template += '<label for="tableOutlineWidth">Толщина внешней границы <input type="text" name="tableOutlineWidth" id="tableOutlineWidth" value=""/></label>';
            template += '<label for="tableOutlineColor">Цвет внешней границы <input type="text" name="tableOutlineColor" id="tableOutlineColor" value=""/></label>';
            template += '<label for="tableOutlineStyle">Стиль границы' +
                '<select name="tableOutlineStyle" id="tableOutlineStyle" value="">' +
                '<option value="none">нет</option>' +
                '<option value="solid">_______</option>' +
                '<option value="dashed">_ _ _ _</option>' +
                '<option value="dotted">. . . . . </option>' +
                '</select></label>';

            template += '</div>';

            template += '<div class="te-row">';

            template += '<label for="tableWidth">Ширина таблицы <input type="text" name="tableWidth" id="tableWidth" value=""/></label>';
            template += '<label for="tableHeight">Высота таблицы <input type="text" name="tableHeight" id="tableHeight" value=""/></label>';

            template += '</div>';

            template += '<label for="tableClear">Очистить оформление таблицы ' +
                '<input type="checkbox" name="tableClear" id="tableClear" style="vertical-align: text-bottom;" /></label>';

            template += '</section>';

            this.modal.addTemplate('tableOptions', template);
            this.modal.load('tableOptions', 'Свойства таблицы', 600);

            this.modal.createCancelButton();

            var button = this.modal.createActionButton("Ok");

            button.on('click', this.tableEdit.setTableOptions);


            this.modal.show();
            this.tableEdit.getTableOptions();
        },
        getTdOptions: function () {
            var wrapper = $("#td-options-wrapper");

            var valign = $(td).attr('valign');

            var backgroundColor = $(td).css("background-color");

            var borderWidth = $(td).css("border-width");
            var borderColor = $(td).css("border-color");
            var borderStyle = $(td).css("border-style");

            var outlineWidth = $(td).css("outline-width");
            var outlineColor = $(td).css("outline-color");
            var outlineStyle = $(td).css("outline-style");

            var colspan = $(td).attr("colspan");
            var rowspan = $(td).attr("rowspan");

            var width = $(td).get(0).style.width ;
            var height = $(td).get(0).style.height;

            $('#tdValign').val(valign);

            $("#tdBackgroundColor").spectrum({color: backgroundColor, showInput: true, showAlpha: true});

            wrapper.find("#tdBorderWidth").val(borderWidth);
            $("#tdBorderColor").spectrum({color: borderColor, showInput: true, showAlpha: true});
            wrapper.find("#tdBorderStyle").val(borderStyle);

            wrapper.find("#tdOutlineWidth").val(outlineWidth);
            $("#tdOutlineColor").spectrum({color: outlineColor, showInput: true, showAlpha: true});
            wrapper.find("#tdOutlineStyle").val(outlineStyle);

            wrapper.find("#tdColspan").val(colspan);
            wrapper.find("#tdRowspan").val(rowspan);

            wrapper.find("#tdWidth").val(width);
            wrapper.find("#tdHeight").val(height);
        },
        getTableOptions: function () {
            var wrapper = $("#table-options-wrapper");

            var backgroundColor = $(table).css("background-color");

            var borderWidth = $(table).css("border-width");
            var borderColor = $(table).css("border-color");
            var borderStyle = $(table).css("border-style");

            var outlineWidth = $(table).css("outline-width");
            var outlineColor = $(table).css("outline-color");
            var outlineStyle = $(table).css("outline-style");

            var width = $(table).css("width");
            var height = $(table).css("height");

            var isCenter = $(table).hasClass('tde-centered');

            console.log($(table).css("margin"));

            $("#tableBackgroundColor").spectrum({color: backgroundColor, showInput: true, showAlpha: true});

            wrapper.find("#tableCenter").prop('checked', isCenter);

            wrapper.find("#tableBorderWidth").val(borderWidth);
            $("#tableBorderColor").spectrum({color: borderColor, showInput: true, showAlpha: true});
            wrapper.find("#tableBorderStyle").val(borderStyle);

            wrapper.find("#tableOutlineWidth").val(outlineWidth);
            $("#tableOutlineColor").spectrum({color: outlineColor, showInput: true, showAlpha: true});
            wrapper.find("#tableOutlineStyle").val(outlineStyle);

            wrapper.find("#tableWidth").val(width);
            wrapper.find("#tableHeight").val(height);
        },
        setTdOptions: function () {
            this.buffer.set();

            var wrapper = $("#td-options-wrapper");

            var isClear = wrapper.find("#tdClear").prop("checked");

            if (isClear) {
                $(td).removeAttr("style")
                    .removeAttr("colspan")
                    .removeAttr("rowspan");
                return false;
            }

            $inputs = $('.touched');

            var tdValign = wrapper.find('#tdValign').hasClass('touched') ? wrapper.find('#tdValign').val() : '';

            var backgroundColor = wrapper.find("#tdBackgroundColor").hasClass('touched')
                ? wrapper.find("#tdBackgroundColor+.sp-replacer .sp-preview-inner").css("background-color") : '';

            var borderWidth = wrapper.find("#tdBorderWidth").hasClass('touched')
                ? wrapper.find("#tdBorderWidth").val() : '';
            var borderColor = wrapper.find("#tdBorderColor").hasClass('touched')
                ? wrapper.find("#tdBorderColor+.sp-replacer .sp-preview-inner").css("background-color") : '';
            var borderStyle = wrapper.find("#tdBorderStyle").hasClass('touched')
                ? wrapper.find("#tdBorderStyle").val() : '';

            var outlineWidth = wrapper.find("#tdOutlineWidth").hasClass('touched')
                ? wrapper.find("#tdOutlineWidth").val() : '';
            var outlineColor = wrapper.find("#tdOutlineColor").hasClass('touched')
                ? wrapper.find("#tdOutlineColor+.sp-replacer .sp-preview-inner").css("background-color") : '';
            var outlineStyle = wrapper.find("#tdOutlineStyle").hasClass('touched')
                ? wrapper.find("#tdOutlineStyle").val() : '';

            var colspan = wrapper.find("#tdColspan").hasClass('touched')
                ? wrapper.find("#tdColspan").val() : '';
            var rowspan = wrapper.find("#tdRowspan").hasClass('touched')
                ? wrapper.find("#tdRowspan").val() : '';

            var width = wrapper.find("#tdWidth").hasClass('touched')
                ? wrapper.find("#tdWidth").val() : '';
            var height = wrapper.find("#tdHeight").hasClass('touched')
                ? wrapper.find("#tdHeight").val() : '';

            tdValign ? $(td).attr('valign', tdValign) : '';

            backgroundColor ? $(td).css("background-color", backgroundColor) : '';

            borderWidth ? $(td).css("border-width", borderWidth) : '';
            borderColor ? $(td).css("border-color", borderColor) : '';
            borderStyle ? $(td).css("border-style", borderStyle) : '';

            outlineWidth ? $(td).css("outline-width", outlineWidth) : '';
            outlineColor ? $(td).css("outline-color", outlineColor) : '';
            outlineStyle ? $(td).css("outline-style", outlineStyle) : '';

            colspan ? $(td).attr("colspan", colspan) : '';
            rowspan ? $(td).attr("rowspan", rowspan) : '';

            width ? $(td).css("width", width).css('display', 'block') : '';
            height ? $(td).css("height", height) : '';

            this.code.sync();

            this.modal.close();
            this.selection.restore();
        },
        setTableOptions: function () {
            this.buffer.set();

            var wrapper = $("#table-options-wrapper");

            var isClear = wrapper.find("#tableClear").prop("checked");

            if (isClear) {
                $(table).removeAttr("style");
                return false;
            }

            var backgroundColor = wrapper.find("#tableBackgroundColor").hasClass('touched')
                ? wrapper.find("#tableBackgroundColor+.sp-replacer .sp-preview-inner").css("background-color") : '';


            var tableAllBorders = wrapper.find("#tableAllBorders").prop('checked');
            var borderWidth = wrapper.find("#tableBorderWidth").hasClass('touched')
                ? wrapper.find("#tableBorderWidth").val() : '';
            var borderColor = wrapper.find("#tableBorderColor").hasClass('touched')
                ? wrapper.find("#tableBorderColor+.sp-replacer .sp-preview-inner").css("background-color") : '';
            var borderStyle = wrapper.find("#tableBorderStyle").hasClass('touched')
                ? wrapper.find("#tableBorderStyle").val() : '';

            var outlineWidth = wrapper.find("#tableOutlineWidth").hasClass('touched')
                ? wrapper.find("#tableOutlineWidth").val() : '';
            var outlineColor = wrapper.find("#tableOutlineColor").hasClass('touched')
                ? wrapper.find("#tableOutlineColor+.sp-replacer .sp-preview-inner").css("background-color") : '';
            var outlineStyle = wrapper.find("#tableOutlineStyle").hasClass('touched')
                ? wrapper.find("#tableOutlineStyle").val() : '';

            var width = wrapper.find("#tableWidth").hasClass('touched') ? wrapper.find("#tableWidth").val() : '';
            var height = wrapper.find("#tableHeight").hasClass('touched') ? wrapper.find("#tableHeight").val() : '';

            var isCenter = wrapper.find("#tableCenter").hasClass('touched')
                ? wrapper.find("#tableCenter").prop("checked") : '';

            backgroundColor ? $(table).css("background-color", backgroundColor) : '';

            borderWidth ? $(table).css("border-width", borderWidth) : '';
            borderColor ? $(table).css("border-color", borderColor) : '';
            borderStyle ? $(table).css("border-style", borderStyle) : '';

            if (tableAllBorders) {
                var tds = $(table).find('td');
                borderWidth ? $(tds).css("border-width", borderWidth) : '';
                borderColor ? $(tds).css("border-color", borderColor) : '';
                borderStyle ? $(tds).css("border-style", borderStyle) : '';
            }

            outlineWidth ? $(table).css("outline-width", outlineWidth) : '';
            outlineColor ? $(table).css("outline-color", outlineColor) : '';
            outlineStyle ? $(table).css("outline-style", outlineStyle) : '';

            width ? $(table).css("width", width) : '';
            height ? $(table).css("height", height) : '';

            if (isCenter) {
                $(table).css('margin', '0 auto').addClass('tde-centered');
            } else {
                $(table).css('margin', 'inherit').removeClass('tde-centered');
            }

            this.code.sync();

            this.modal.close();
            this.selection.restore();
        },
        getTable: function () {
            var $table = $(this.selection.getParent()).closest('table');

            if (!this.utils.isRedactorParent($table)) return false;
            if ($table.size() === 0) return false;

            return $table;
        },
        getTd: function () {
            var nodes = this.selection.getNodes();
            var $td = [];

            nodes.forEach(function (_item) {
                if (_item.nodeName === 'TD') {
                    $td.push(_item);
                }
            });

            if (!$td.length) {
                var node = $(nodes[0]).parents('td')[0];
                if (node.nodeName === 'TD') {
                    $td.push(node);
                }
            }

            if (!$td.length) return false;

            return $td;
        },
        tdSpan: function () {
            this.buffer.set();

            td = this.tableEdit.getTd();
            if (!td) {
                return false;
            }

            $(td[0]).attr('colspan', td.length);
            for (var i = 1; i < td.length; i++) {
                $(td[i]).remove();
            }

            this.code.sync();
        },
        tdSpanDel: function () {
            this.buffer.set();

            td = this.tableEdit.getTd();
            if (!td) {
                return false;
            }

            td.forEach(function (_td) {
                var colspan = $(_td).attr('colspan');
                if (colspan) {
                    $(_td).removeAttr('colspan');
                    for (var i = 0; i < colspan - 1; i++) {
                        $('<td>').insertAfter($(_td));
                    }
                }
            });

            this.code.sync();
        },
        tdDel: function () {
            this.buffer.set();

            td = this.tableEdit.getTd();
            if (!td) {
                return false;
            }
            $(td).remove();

            this.code.sync();
        },
        tdSpanLeft: function () {
            this.buffer.set();

            this.tableEdit.tdColspan('left');

            this.code.sync();
        },
        tdSpanRight: function () {
            this.buffer.set();

            this.tableEdit.tdColspan('right');

            this.code.sync();
        },
        tdSpanTop: function () {
            this.buffer.set();

            this.tableEdit.tdRowspan('top');

            this.code.sync();
        },
        tdSpanBottom: function () {
            this.buffer.set();

            this.tableEdit.tdRowspan('bottom');

            this.code.sync();
        },
        tdColspan: function(direction) {
            direction = direction || 'left';

            td = this.tableEdit.getTd();
            if (!td) {
                return false;
            }

            var elem = $(td[0]);
            var elem2 = (direction === 'left') ? $(elem).prev('td') : $(elem).next('td');

            if (elem2.length) {
                var colspanElem2 = +elem2.attr('colspan') || 1;
                var colspanElem = +elem.attr('colspan') || 1;
                var rowspanElem = +elem.attr('rowspan') || 1;
                var rowspanElem2 = +elem2.attr('rowspan') || 1;

                if (rowspanElem2 !== rowspanElem) {
                    console.log('rowspanElem2 !== rowspanElem', rowspanElem2, rowspanElem);
                    return false;
                }

                var colspan = colspanElem2 + colspanElem;

                if (colspan < 2) {
                    colspan = 2;
                }

                $(elem).attr('colspan', colspan);
                $(elem2).remove();
            } else {
                return false;
            }
        },
        tdRowspan: function (direction) {
            direction = direction || 'top';

            td = this.tableEdit.getTd();
            if (!td) {
                return false;
            }

            var elem = $(td[0]);
            var colspanElem = +elem.attr('colspan') || 1;
            var rowspanElem = +elem.attr('rowspan') || 1;
            var elem2;

            if (direction === 'top') {
                var b = elem.get(0).getBoundingClientRect();
                var elem2 = $(document.elementFromPoint(b.left, b.top - 10));
            } else {
                var b = elem.get(0).getBoundingClientRect();
                var elem2 = $(document.elementFromPoint(b.left, b.bottom + 10));
            }

            if (!elem2) {
                console.log('elem2 not found');
                return false;
            }

            var colspanElem2 = +elem2.attr('colspan') || 1;
            var rowspanElem2 = +elem2.attr('rowspan') || 1;

            if (colspanElem !== colspanElem2) {
                console.log('colspanElem !== colspanElem2', colspanElem2, colspanElem);
                return false;
            }

            var rowspan = rowspanElem + rowspanElem2;

            if (direction === 'top') {
                elem2.attr('rowspan', rowspan);
                elem.remove();
            } else {
                elem.attr('rowspan', rowspan);
                elem2.remove();
            }
        }
    };
};