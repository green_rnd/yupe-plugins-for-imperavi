/*
 Spoiler plugin for imperavi editor and Yupe CMS
 Andrey Balyasov
 green.rnd@ya.ru
 */
if (!RedactorPlugins) var RedactorPlugins = {};

(function ($) {
    RedactorPlugins.spoiler = function () {
        return {
            init: function () {
                var that = this;
                console.log("spoiler init");
                var button = this.button.add('spoiler', 'Spoiler');
                this.button.addCallback(button, this.spoiler.setSpoiler);
            },
            setSpoiler: function () {
                this.selection.get();

                var startEl = this.range.startContainer;
                var endEl = this.range.endContainer;

                this.caret.setBefore(startEl);
                this.insert.text('[spoiler]');
                this.caret.setAfter(endEl);
                this.insert.text('[/spoiler]');

                this.selection.restore();
                this.code.sync();
            }
        };
    };
})(jQuery);